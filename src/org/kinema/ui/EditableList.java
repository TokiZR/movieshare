package org.kinema.ui;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EventObject;
import java.util.List;

import javax.swing.AbstractCellEditor;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.EventListenerList;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import net.miginfocom.swing.MigLayout;

public class EditableList<T extends Comparable<? super T>> extends JPanel {

	/**
	 * Serial version ID
	 */
	private static final long serialVersionUID = 6658694757726633934L;

	private ListModel listModel;
	private Class<T> cls;
	private JTable table;
	private Font smallFont;

	private ListCellEditorRenderer<T> cellEditor = null;
	private JPanel editor;
	private JPanel renderer;
	private ListEditorRenderer editorRenderer;

	public EditableList(Class<T> modelClass) {
		super(new MigLayout("wrap, fill"));

		cls = modelClass;

		smallFont = UIManager.getDefaults().getFont("Label.font").deriveFont(10.0f);

		listModel = new ListModel();

		editor = createCell();
		renderer = createCell();

		table = new JTable(listModel);
		table.setTableHeader(null);
		add(table, "grow 1 1");

		final JButton addButton = new JButton("Add");
		addButton.setFont(smallFont);
		addButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
				Object source = ae.getSource();
				if (source == addButton) {
					try {
						listModel.add(cls.newInstance());
					} catch (InstantiationException | IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});

		add(addButton, "dock north, alignx 50%");
	}

	private JPanel createCell() {
		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.LINE_AXIS));

		JButton button = new JButton("Remove");
		button.setFont(smallFont);
		button.addActionListener(new ActionListener() {

			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent e) {
				JTable table = (JTable) SwingUtilities.getAncestorOfClass(JTable.class,
						(Component) e.getSource());
				int row = table.getEditingRow();
				table.getCellEditor().stopCellEditing();
				((ListModel) table.getModel()).remove(row);
			}
		});

		p.add(button);

		return p;
	}

	public ListCellEditorRenderer<T> getCellEditor() {
		return cellEditor;
	}

	public void setCellEditor(ListCellEditorRenderer<T> cellEditor) {
		this.cellEditor = cellEditor;

		editor.add(cellEditor.getEditor(null, -1), 0);
		renderer.add(cellEditor.getRenderer(null, -1), 0);
		table.setRowHeight(Math.max(cellEditor.getEditor(null, -1).getPreferredSize().height,
				cellEditor.getRenderer(null, -1).getPreferredSize().height));

		editorRenderer = new ListEditorRenderer();

		table.setDefaultRenderer(listModel.getColumnClass(0), editorRenderer);
		table.setDefaultEditor(listModel.getColumnClass(0), editorRenderer);
	}

	public List<T> getData() {
		return listModel.getData();
	}

	public void setData(List<T> data) {
		listModel.setData(data);
	}

	public TableModel getModel() {
		return listModel;
	}

	private class ListEditorRenderer extends AbstractCellEditor implements TableCellRenderer,
			TableCellEditor {

		/**
		 * 
		 */
		private static final long serialVersionUID = -8507642168039328110L;

		@Override
		public Object getCellEditorValue() {
			return cellEditor.getEditorValue();
		}

		@SuppressWarnings("unchecked")
		@Override
		public Component getTableCellEditorComponent(JTable table, Object value,
				boolean isSelected, int row, int column) {
			cellEditor.getEditor((T) value, row);
			return editor;
		}

		@SuppressWarnings("unchecked")
		@Override
		public Component getTableCellRendererComponent(JTable table, Object value,
				boolean isSelected, boolean hasFocus, int row, int column) {
			cellEditor.getRenderer((T) value, row);
			return renderer;
		}

		@Override
		public boolean isCellEditable(EventObject anEvent) {
			return true;
		}

		@Override
		public boolean shouldSelectCell(EventObject anEvent) {
			return false;
		}

	}

	private class ListModel implements TableModel {

		protected EventListenerList listenerList;

		private ArrayList<T> data;

		public ListModel() {
			listenerList = new EventListenerList();
			data = new ArrayList<>();
		}

		public void add(T item) {
			data.add(item);
			fireTableRowsInserted(data.size() - 1, data.size() - 1);
		}

		public T remove(int row) {
			if (row < data.size()) {
				fireTableRowsDeleted(row, row);
				return data.remove(row);
			}
			return null;
		}

		@SuppressWarnings("unused")
		public boolean remove(T el) {
			int row = data.indexOf(el);
			if (row >= 0)
				fireTableRowsDeleted(row, row);
			return data.remove(el);
		}

		@SuppressWarnings("unused")
		public void sort() {
			Collections.sort(data);
			fireTableDataChanged();
		}

		public List<T> getData() {
			return Collections.unmodifiableList(data);
		}

		public void setData(List<T> data) {
			this.data = new ArrayList<>(data);
			fireTableDataChanged();
		}

		@Override
		public int getColumnCount() {
			return 1;
		}

		@Override
		public void addTableModelListener(TableModelListener l) {
			this.listenerList.add(TableModelListener.class, l);
		}

		@Override
		public void removeTableModelListener(TableModelListener l) {
			this.listenerList.remove(TableModelListener.class, l);
		}

		@Override
		public Class<?> getColumnClass(int paramInt) {
			if (paramInt == 0)
				return cls;
			else
				return null;
		}

		@Override
		public String getColumnName(int paramInt) {
			return "";
		}

		@Override
		public int getRowCount() {
			return data.size();
		}

		@Override
		public Object getValueAt(int row, int col) {
			if (col == 0)
				return data.get(row);
			else
				return null;
		}

		@Override
		public boolean isCellEditable(int paramInt1, int paramInt2) {
			return true;
		}

		@SuppressWarnings("unchecked")
		@Override
		public void setValueAt(Object paramObject, int row, int col) {
			if (col == 0 && row >= 0 && row < data.size()) {
				data.set(row, (T) paramObject);
				fireTableCellUpdated(row, col);
			}
		}

		public void fireTableDataChanged() {
			fireTableChanged(new TableModelEvent(this));
		}

		public void fireTableRowsInserted(int firstRow, int lastRow) {
			fireTableChanged(new TableModelEvent(this, firstRow, lastRow, -1, 1));
		}

		public void fireTableRowsDeleted(int firstRow, int lastRow) {
			fireTableChanged(new TableModelEvent(this, firstRow, lastRow, -1, -1));
		}

		public void fireTableCellUpdated(int row, int column) {
			fireTableChanged(new TableModelEvent(this, row, row, column));
		}

		public void fireTableChanged(TableModelEvent e) {
			Object[] listeners = this.listenerList.getListenerList();

			for (int i = listeners.length - 2; i >= 0; i -= 2)
				if (listeners[i] == TableModelListener.class)
					((TableModelListener) listeners[(i + 1)]).tableChanged(e);
		}
	}
}
