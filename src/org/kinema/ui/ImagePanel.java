package org.kinema.ui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

import org.kinema.image.ImageCache;
import org.kinema.image.ImageLoadedListener;

public class ImagePanel extends JComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BufferedImage image = null;

	private Dimension imageSize;

	public static final int SIZE_PROPORTIONAL = -1;

	public Dimension getImageSize() {
		return imageSize;
	}

	public void setImageSize(Dimension imageSize) {
		if (imageSize.width < 0 && imageSize.height < 0)
			this.imageSize = null;
		else
			this.imageSize = new Dimension(imageSize);
	}

	public void setImageSize(int width, int height) {
		if (width < 0 && height < 0)
			this.imageSize = null;
		else
			this.imageSize = new Dimension(width, height);
	}

	public ImagePanel(File path) throws IOException {
		if (path != null)
			image = ImageIO.read(path);
	}

	public ImagePanel(URL url) throws IOException {
		if (url != null)
			ImageCache.getDefault().getImage(url, loadListener);
	}

	public ImagePanel() {
	}

	public void setFromFile(File path) throws IOException {
		if (path != null)
			image = ImageIO.read(path);
		else
			image = null;
	}

	public void setFromUrl(URL url) throws IOException {
		if (url != null)
			ImageCache.getDefault().getImage(url, loadListener);
		else
			image = null;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (image != null) {
			if (imageSize != null) {
				int w, h;
				if (imageSize.width < 0) {
					w = (imageSize.height * image.getWidth()) / image.getHeight();
				} else {
					w = imageSize.width;
				}

				if (imageSize.height < 0) {
					h = (imageSize.width * image.getHeight()) / image.getWidth();
				} else {
					h = imageSize.height;
				}

				g.drawImage(image, 0, 0, w, h, null);
			} else {
				g.drawImage(image, 0, 0, null);
			}
		}
	}

	@Override
	public Dimension getPreferredSize() {
		if (imageSize != null) {
			int w, h;

			if (image != null) {
				if (imageSize.width < 0) {
					w = (imageSize.height * image.getWidth()) / image.getHeight();
				} else {
					w = imageSize.width;
				}

				if (imageSize.height < 0) {
					h = (imageSize.width * image.getHeight()) / image.getWidth();
				} else {
					h = imageSize.height;
				}

				return new Dimension(w, h);
			} else {
				return new Dimension(imageSize);
			}
		} else {
			if (image != null)
				return new Dimension(image.getWidth(), image.getHeight());
			else
				return new Dimension(0, 0);
		}
	}

	private ImageLoadedListener loadListener = new ImageLoadedListener() {

		@Override
		public void imageLoaded(BufferedImage im) {
			image = im;
			revalidate();
			repaint();
		}

		@Override
		public void exception(Exception e) {
			//e.printStackTrace();
		}
	};
}
