package org.kinema.ui.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import org.kinema.KApplication;
import org.kinema.Settings;
import org.kinema.util.NetUtils;

public class PastryBootstrapDialog extends KDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7326663412277422403L;

	private JComboBox<String> localAddress;
	private JTextField localPort;
	private JLabel status;
	private JCheckBox autoBoot;

	private Settings settings;

	public PastryBootstrapDialog(KApplication app) {
		super(app, "Bootstrap");

		settings = app.getSettings();

		List<String> hosts = NetUtils.getLocalAddressNames();
		localAddress = new JComboBox<String>(hosts.toArray(new String[hosts.size()]));
		localAddress.setEditable(true);
		localAddress
				.setToolTipText("The address to which the local socket will be bound, this address also identifies the node on the netwrk so it should be consistent with what other nodes will see.");
		add(new JLabel("Local Address"));
		add(localAddress, "width 170");

		localPort = new JTextField();
		add(new JLabel("Local Port"));
		add(localPort, "width 100");

		autoBoot = new JCheckBox("Boot on Startup");
		autoBoot.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				settings.setAutoBoot(autoBoot.isSelected());
			}
		});
		add(autoBoot, "skip");

		status = new JLabel();
		add(new JLabel("Status"));
		add(status);

		JButton boot = new JButton("Boot");
		boot.addActionListener(connectPastry);
		add(boot, "skip, split2, align right");

		JButton close = new JButton("Close");
		close.addActionListener(closeDialog);
		add(close, "align right");

		open();
	}

	@Override
	protected void updateFields() {
		localAddress.setSelectedItem(settings.getLocalHost());
		localPort.setText(portToString(settings.getLocalPort()));
		status.setText(app.getConnStatus());
		autoBoot.setSelected(settings.getAutoBoot());
		revalidate();
		pack();
	}

	String portToString(int port) {
		if (port <= 0) {
			return "";
		} else {
			return Integer.toString(port);
		}
	}

	private ActionListener connectPastry = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent paramActionEvent) {
			settings.setLocalHost(localAddress.getSelectedItem().toString());
			settings.setLocalPort(Integer.parseInt(enforceValue(localPort.getText(), "0")));

			try {
				app.bootPastry();
				updateFields();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(PastryBootstrapDialog.this, e.getLocalizedMessage(),
						"Error", JOptionPane.ERROR_MESSAGE);
			}

			updateFields();
		}
	};
}
