package org.kinema.ui.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import org.kinema.KApplication;
import org.kinema.Settings;
import org.kinema.util.NetUtils;

public class PastryConnectionDialog extends KDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1498723976239L;

	private JTextField remoteAddress;
	private JTextField remotePort;
	private JComboBox<String> localAddress;
	private JTextField localPort;
	private JLabel status;
	private JCheckBox autoConnect;

	private Settings settings;

	public PastryConnectionDialog(KApplication app) {
		super(app, "Connect");

		settings = app.getSettings();

		remoteAddress = new JTextField();
		add(new JLabel("Remote Address"));
		add(remoteAddress, "width 170");

		remotePort = new JTextField();
		add(new JLabel("Remote Port"));
		add(remotePort, "width 100");

		List<String> hosts = NetUtils.getLocalAddressNames();
		localAddress = new JComboBox<String>(hosts.toArray(new String[hosts.size()]));
		localAddress.setEditable(true);
		localAddress
		.setToolTipText("The address to which the local socket will be bound, this address also identifies the node on the netwrk so it should be consistent with what other nodes will see.");
		add(new JLabel("Local Address"));
		add(localAddress, "width 170");

		localPort = new JTextField();
		add(new JLabel("Local Port"));
		add(localPort, "width 100");

		autoConnect = new JCheckBox("Connect on Startup");
		autoConnect.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				settings.setAutoConnect(autoConnect.isSelected());
			}
		});
		add(autoConnect, "skip");

		status = new JLabel();
		add(new JLabel("Status"));
		add(status);

		JButton connect = new JButton("Connect");
		connect.addActionListener(connectPastry);
		add(connect, "skip, split 2, align right");

		JButton close = new JButton("Close");
		close.addActionListener(closeDialog);
		add(close, "align right");

		open();
	}

	@Override
	protected void updateFields() {
		remoteAddress.setText(settings.getRemoteHost());
		remotePort.setText(portToString(settings.getRemotePort()));
		localAddress.setSelectedItem(settings.getLocalHost());
		localPort.setText(portToString(settings.getLocalPort()));
		status.setText(app.getConnStatus());
		autoConnect.setSelected(settings.getAutoConnect());
		revalidate();
		pack();
	}

	String portToString(int port) {
		if (port <= 0) {
			return "";
		} else {
			return Integer.toString(port);
		}
	}

	private ActionListener connectPastry = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent paramActionEvent) {
			settings.setRemoteHost(remoteAddress.getText());
			settings.setRemotePort(Integer.parseInt(enforceValue(remotePort.getText(), "0")));
			settings.setLocalHost(localAddress.getSelectedItem().toString());
			settings.setLocalPort(Integer.parseInt(enforceValue(localPort.getText(), "0")));

			try {
				app.connectPastry();
				updateFields();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(PastryConnectionDialog.this, e.getLocalizedMessage(),
						"Error", JOptionPane.ERROR_MESSAGE);
			}

			status.setText(app.getConnStatus());
		}
	};
}
