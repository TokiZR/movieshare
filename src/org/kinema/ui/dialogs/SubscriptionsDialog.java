package org.kinema.ui.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;

import org.apache.commons.lang3.StringUtils;
import org.kinema.KApplication;
import org.kinema.model.Genre;
import org.kinema.model.Subscriptions;

public class SubscriptionsDialog extends KDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2735988713514999802L;

	private JTextPane genres;
	private GenreEditor ge;
	private JTextArea tagsField;

	private Subscriptions subs;
	private ArrayList<Genre> genreCache;

	public SubscriptionsDialog(KApplication app) {
		super(app, "Subscription Settings");

		subs = app.getSubscriptions();
		genreCache = new ArrayList<>(subs.genres);

		genres = new JTextPane();
		genres.setEditable(false);
		genres.setBackground(null);
		genres.setOpaque(false);
		ge = new GenreEditor(genreCache,
				(JFrame) SwingUtilities.getWindowAncestor(this), true);
		JButton editGenres = new JButton("Edit");
		editGenres.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				ge.setVisible(true);
			}
		});
		ge.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent paramActionEvent) {
				if (genreCache.size() == Genre.values().length)
					genres.setText("Any");
				else if (genreCache.size() == 0)
					genres.setText("None");
				else
					genres.setText(StringUtils.join(genreCache, ", "));
				pack();
			}
		});
		add(new JLabel("Genres"));
		add(genres, "split 2, growx");
		add(editGenres);

		tagsField = new JTextArea();
		tagsField.setLineWrap(true);
		tagsField.setBorder(BorderFactory.createLoweredBevelBorder());
		tagsField.setToolTipText("A comma separated list of tags");
		add(new JLabel("Tags"));
		add(tagsField, "w 200:200:, h 50::, growx");

		JButton done = new JButton("Done");
		done.addActionListener(closeDialog);
		add(done, "skip 1, align right");

		open();
	}

	@Override
	protected void updateFields() {
		if (subs.genres.size() == Genre.values().length)
			genres.setText("Any");
		else if (subs.genres.size() == 0)
			genres.setText("None");
		else
			genres.setText(StringUtils.join(subs.genres, ", "));

		tagsField.setText(StringUtils.join(subs.tags, ", "));
		pack();
	}

	private ActionListener closeDialog = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent paramActionEvent) {
			String tags[] = tagsField.getText().split(",");
			for (int i = 0; i < tags.length; i++) {
				tags[i] = tags[i].trim();
			}

			subs.tags = new ArrayList<String>(Arrays.asList(tags));
			subs.genres = genreCache;
			setVisible(false);
			dispose();
		}
	};
}
