package org.kinema.ui.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

import org.kinema.KApplication;
import org.kinema.ui.panels.MovieDisplay;
import org.kinema.ui.panels.MovieEditor;

public class CreateMovieDialog extends KDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -390846448157203052L;

	private MovieEditor editor;
	private MovieDisplay display;

	public CreateMovieDialog(KApplication app) {
		super(app, "Create Movie");
		setResizable(true);
		setLayout(new MigLayout("wrap, fill", "[::450][::350]"));

		JPanel buttons = new JPanel(new MigLayout("fill", "[]push[]"));
		
		final JCheckBox preview = new JCheckBox("Preview");
		preview.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent paramActionEvent) {
				display.setVisible(preview.isSelected());
				pack();
			}
		});
		buttons.add(preview, "split 2");

		JButton updatePreview = new JButton("Update Preview");
		updatePreview.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent paramActionEvent) {
				display.setMovie(editor.getMovie());
				if(display.isVisible()) {
					pack();
				}
			}
		});
		buttons.add(updatePreview);

		JButton done = new JButton("Create");
		done.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent paramActionEvent) {
				CreateMovieDialog.this.app.publishMovie(editor.getMovie());
				setVisible(false);
				dispose();
			}
		});
		buttons.add(done, "");
		
		add(buttons, "north");
		
		editor = new MovieEditor();
		add(editor);
		
		display = new MovieDisplay(null);
		add(display, "hidemode 3");
		display.setVisible(false);

		editor.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent event) {
				pack();
			}
		});
		
		open();
	}
}
