package org.kinema.ui.dialogs;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.KeyStroke;

import net.miginfocom.swing.MigLayout;

import org.kinema.KApplication;

public class KDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7759094969270928694L;

	protected KApplication app;

	public KDialog(KApplication app, String title) {
		super(app.getMainWindow(), Dialog.ModalityType.APPLICATION_MODAL);
		setTitle(title);
		setResizable(false);

		setLayout(new MigLayout("wrap", "[80][::400]"));

		this.app = app;

		getRootPane()
				.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
				.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
						"closeDialog");
		getRootPane().getActionMap().put("closeDialog",
				new AbstractAction("closeDialog") {

					/**
			 * 
			 */
					private static final long serialVersionUID = -4065608256178981576L;

					@Override
					public void actionPerformed(ActionEvent event) {
						closeDialog.actionPerformed(event);
					}
				});

	}

	protected void open() {
		updateFields();
		setLocationRelativeTo(app.getMainWindow());
		setVisible(true);
	}

	protected void updateFields() {
		pack();
	}

	protected String enforceValue(String value, String deflt) {
		if (value == null || value.trim().equals("")) {
			return deflt;
		} else {
			return value;
		}
	}

	protected ActionListener closeDialog = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent paramActionEvent) {
			setVisible(false);
			dispose();
		}
	};
}
