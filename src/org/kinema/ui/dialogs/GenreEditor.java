package org.kinema.ui.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.event.EventListenerList;

import net.miginfocom.swing.MigLayout;

import org.kinema.model.Genre;

public class GenreEditor extends JDialog {

	protected EventListenerList listenerList = new EventListenerList();

	public void addActionListener(ActionListener listener) {
		listenerList.add(ActionListener.class, listener);
	}

	public void removeActionListener(ActionListener listener) {
		listenerList.remove(ActionListener.class, listener);
	}

	void fireActionEvent(ActionEvent evt) {
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i + 2) {
			if (listeners[i] == ActionListener.class) {
				((ActionListener) listeners[i + 1]).actionPerformed(evt);
			}
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 3785532836497300379L;

	private JCheckBox genres[];

	private ArrayList<Genre> genreList;

	public GenreEditor(ArrayList<Genre> genreArray, JFrame parentWindow,
			boolean showExtraButtons) {
		super(parentWindow, true);
		genreList = genreArray;

		setTitle("Genres");
		setLayout(new MigLayout("wrap"));

		JPanel checkboxes = new JPanel(new MigLayout("wrap, gap 0", "[][][]"));

		genres = new JCheckBox[Genre.values().length];

		for (int i = 0; i < genres.length; i++) {
			genres[i] = new JCheckBox(Genre.values()[i].toString(),
					genreList.contains(Genre.values()[i]));
			genres[i].putClientProperty("genre", Genre.values()[i]);
			genres[i].addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					JCheckBox source = (JCheckBox) event.getSource();

					if (source != null) {
						Genre g = (Genre) source.getClientProperty("genre");
						if (genreList.contains(g)) {
							genreList.remove(g);
						} else {
							genreList.add(g);
						}
					}
				}
			});
			checkboxes.add(genres[i]);
		}

		add(checkboxes);

		JPanel buttons = new JPanel(new MigLayout("fill", "[][]push[]"));

		if (showExtraButtons) {
			JButton selectAll = new JButton("Select All");
			selectAll.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent paramActionEvent) {
					for (JCheckBox cb : genres) {
						cb.setSelected(true);
					}

					genreList.clear();
					for (int i = 0; i < genres.length; i++) {
						genreList.add(Genre.values()[i]);
					}
				}
			});
			buttons.add(selectAll, "");

			JButton selectNone = new JButton("Select None");
			selectNone.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent paramActionEvent) {
					for (JCheckBox cb : genres) {
						cb.setSelected(false);
					}
					genreList.clear();
				}
			});
			buttons.add(selectNone, "");
		}
		
		JButton doneButton = new JButton("Done");
		doneButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				fireActionEvent(new ActionEvent(this,
						ActionEvent.ACTION_PERFORMED, ""));
				setVisible(false);
			}
		});
		buttons.add(doneButton, "cell 2 0");

		add(buttons, "growx");

		pack();
		setLocationRelativeTo(parentWindow);
	}

	public void listChanged(ArrayList<Genre> newList) {
		if (newList != null)
			genreList = newList;
		for (int i = 0; i < genres.length; i++) {
			genres[i].setSelected(genreList.contains(Genre.values()[i]));
		}
	}
}
