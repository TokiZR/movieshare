package org.kinema.ui;

import java.util.Date;

import javax.swing.JComponent;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerModel;
import javax.swing.UIManager;

public class DateListCellEditorRenderer implements ListCellEditorRenderer<Date> {

	private JSpinner eSpin;
	private SpinnerModel eModel;
	private SpinnerModel rModel;
	private JSpinner rSpin;

	public DateListCellEditorRenderer() {
		eModel = new SpinnerDateModel();
		eSpin = new JSpinner(eModel);
		eSpin.setFont(UIManager.getDefaults().getFont("Label.font").deriveFont(10.0f));
		eSpin.setEditor(new JSpinner.DateEditor(eSpin, "HH:mm dd/MM/yyyy"));

		rModel = new SpinnerDateModel();
		rSpin = new JSpinner(rModel);
		rSpin.setFont(UIManager.getDefaults().getFont("Label.font").deriveFont(10.0f));
		rSpin.setEditor(new JSpinner.DateEditor(rSpin, "HH:mm dd/MM/yyyy"));
	}

	@Override
	public Date getEditorValue() {
		return (Date) eModel.getValue();
	}

	@Override
	public JComponent getRenderer(Date val, int index) {
		if (val != null)
			rModel.setValue(val);
		return rSpin;
	}

	@Override
	public JComponent getEditor(Date val, int index) {
		if (val != null)
			eModel.setValue(val);
		return eSpin;
	}

}
