package org.kinema.ui;

import javax.swing.JComponent;

public interface ListCellEditorRenderer<T extends Comparable<? super T>> {
	
	public T getEditorValue();

	public JComponent getRenderer(T val, int index);
	
	public JComponent getEditor(T val, int index);
	
}
