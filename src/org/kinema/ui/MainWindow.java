package org.kinema.ui;

import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.border.BevelBorder;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import net.miginfocom.swing.MigLayout;

import org.kinema.KApplication;
import org.kinema.StatusEvent;
import org.kinema.StatusEventListener;
import org.kinema.model.Movie;
import org.kinema.ui.ScrollablePanel.ScrollableSizeHint;
import org.kinema.ui.dialogs.CreateMovieDialog;
import org.kinema.ui.dialogs.PastryBootstrapDialog;
import org.kinema.ui.dialogs.PastryConnectionDialog;
import org.kinema.ui.dialogs.SubscriptionsDialog;
import org.kinema.ui.panels.MovieDisplay;

public class MainWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 547594498053801896L;

	private ListModel<Movie> movies;

	private ScrollablePanel listPane;
	private ArrayList<MovieDisplay> panels;

	private JLabel statusBar;

	private KApplication app;

	public MainWindow(KApplication application) throws HeadlessException {
		setTitle("Kinema");
		setSize(550, 400);
		setLayout(new MigLayout("fill"));

		this.app = application;

		setupMenu();

		movies = new DefaultListModel<Movie>();

		listPane = new ScrollablePanel();
		listPane.setLayout(new MigLayout("fill, wrap"));
		listPane.setScrollableWidth(ScrollableSizeHint.FIT);

		panels = new ArrayList<>();

		JScrollPane panel = new JScrollPane(listPane, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		add(panel, "growx, growy");

		statusBar = new JLabel(app.getConnStatus());
		statusBar.setBorder(new BevelBorder(BevelBorder.LOWERED));
		add(statusBar, "south");

		movies.addListDataListener(listListener);

		app.addStatusEventListener(new StatusEventListener() {

			@Override
			public void onStatusChange(StatusEvent event) {
				statusBar.setText(app.getConnStatus());
			}
		});
	}

	private void setupMenu() {
		JMenuBar mainMenuBar = new JMenuBar();

		/*******
		 * File menu
		 */

		JMenu file = new JMenu("File");
		file.setMnemonic(KeyEvent.VK_F);

		JMenuItem createMovie = new JMenuItem("Create Movie");
		createMovie.setMnemonic(KeyEvent.VK_M);
		createMovie.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent paramActionEvent) {
				new CreateMovieDialog(app);
			}
		});

		JMenuItem exit = new JMenuItem("Exit");
		exit.setMnemonic(KeyEvent.VK_E);
		exit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent paramActionEvent) {
				app.quit();
			}
		});

		file.add(createMovie);
		file.add(exit);

		/*****
		 * Settings menu
		 */

		JMenu settings = new JMenu("Settings");
		settings.setMnemonic(KeyEvent.VK_T);

		JMenuItem subscriptions = new JMenuItem("Subscriptions");
		subscriptions.setMnemonic(KeyEvent.VK_B);
		subscriptions.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent paramActionEvent) {
				new SubscriptionsDialog(app);
			}
		});

		settings.add(subscriptions);

		/******
		 * Connection menu.
		 */

		JMenu connection = new JMenu("Connection");
		connection.setMnemonic(KeyEvent.VK_C);

		JMenuItem connect = new JMenuItem("Connect...");
		connect.setMnemonic(KeyEvent.VK_C);
		connect.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new PastryConnectionDialog(app);
			}
		});

		JMenuItem bootstrap = new JMenuItem("Bootstrap...");
		bootstrap.setMnemonic(KeyEvent.VK_B);
		bootstrap.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent paramActionEvent) {
				new PastryBootstrapDialog(app);
			}
		});

		connection.add(connect);
		connection.add(bootstrap);

		/******
		 * Final setup.
		 */

		mainMenuBar.add(file);
		mainMenuBar.add(connection);
		mainMenuBar.add(settings);

		setJMenuBar(mainMenuBar);
	}

	public ListModel<Movie> getMovies() {
		return movies;
	}

	private ListDataListener listListener = new ListDataListener() {

		@Override
		public void intervalRemoved(ListDataEvent event) {
			for (int i = event.getIndex0(); i <= event.getIndex1(); i++) {
				listPane.remove(panels.remove(event.getIndex0()));
			}
			revalidate();
			repaint();
		}

		@Override
		public void intervalAdded(ListDataEvent event) {
			for (int i = event.getIndex0(); i <= event.getIndex1(); i++) {
				MovieDisplay p = new MovieDisplay(movies.getElementAt(i));
				JButton deleteButton = new JButton("X");
				deleteButton.setForeground(Color.RED);
				deleteButton.addActionListener(new DeleteAction(movies.getElementAt(i)));
				p.add(deleteButton, "east, growy 0, top, h ::20");
				listPane.add(p, "growx, growy", i);
				panels.add(i, p);
			}
			revalidate();
			repaint();
		}

		@Override
		public void contentsChanged(ListDataEvent event) {
			for (int i = event.getIndex0(); i <= event.getIndex1(); i++) {
				panels.get(i).setMovie(movies.getElementAt(i));
			}
			revalidate();
			repaint();
		}

		class DeleteAction implements ActionListener {
			private Movie movie;

			public DeleteAction(Movie m) {
				movie = m;
			}

			@Override
			public void actionPerformed(ActionEvent paramActionEvent) {
				((DefaultListModel<Movie>) movies).removeElement(movie);
			}
		}
	};
}
