package org.kinema.ui.panels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import net.miginfocom.swing.MigLayout;

import org.apache.commons.lang3.StringUtils;
import org.kinema.model.Movie;
import org.kinema.model.MovieBuilder;
import org.kinema.ui.DateListCellEditorRenderer;
import org.kinema.ui.EditableList;
import org.kinema.ui.dialogs.GenreEditor;

public class MovieEditor extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5379327644797440268L;

	private MovieBuilder builder;

	/**********************************
	 * Components
	 */

	private JTextField title;
	private JTextArea synopsis;
	private EditableList<Date> exhibitions;
	private GenreEditor ge;
	private JTextArea venue;
	private JTextField posterUrl;
	private JLabel genres;
	private JTextArea tags;
	private JTextField IMDBLink;

	public MovieEditor() {
		builder = new MovieBuilder();

		MigLayout layout = new MigLayout("wrap", "[80, right][grow]", "");
		setLayout(layout);

		title = new JTextField();
		add(new JLabel("Title"));
		add(title, "w 150::, grow");

		synopsis = new JTextArea();
		synopsis.setLineWrap(true);
		synopsis.setBorder(BorderFactory.createLoweredBevelBorder());
		add(new JLabel("Synopsis"));
		add(synopsis, "w 250::,h 130!, grow");

		venue = new JTextArea();
		venue.setLineWrap(true);
		venue.setBorder(BorderFactory.createLoweredBevelBorder());
		venue.setToolTipText("Where the movie will be played.");
		add(new JLabel("Venue"));
		add(venue, "w 230!, h 50::");

		exhibitions = new EditableList<>(Date.class);
		exhibitions.setCellEditor(new DateListCellEditorRenderer());
		exhibitions.setToolTipText("Dates and times for the exhibitions");
		add(new JLabel("<html>Exhibition<br/>Dates</html>"));
		add(exhibitions, "w 200!, grow");

		posterUrl = new JTextField();
		posterUrl
				.setToolTipText("Url for a picture of the poster for the movie");
		add(new JLabel("PosterUrl"));
		add(posterUrl, "w 250::, grow");

		genres = new JLabel();
		ge = new GenreEditor(builder.getGenres(),
				(JFrame) SwingUtilities.getWindowAncestor(this), false);
		JButton editGenres = new JButton("Edit");
		editGenres.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				ge.setVisible(true);
			}
		});
		ge.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent paramActionEvent) {
				genres.setText(StringUtils.join(builder.getGenres(), ", "));
			}
		});
		add(new JLabel("Genres"));
		add(genres, "split 2, growx");
		add(editGenres);

		tags = new JTextArea();
		tags.setLineWrap(true);
		tags.setBorder(BorderFactory.createLoweredBevelBorder());
		tags.setToolTipText("A comma separated list of tags");
		add(new JLabel("Tags"));
		add(tags, "w 200!, h 50::");

		IMDBLink = new JTextField();
		IMDBLink.setToolTipText("A link to the IMDB page for the movie if any");
		add(new JLabel("IMDBLink"));
		add(IMDBLink, "w 250::, grow");
	}

	@Override
	protected void processComponentEvent(ComponentEvent e) {
		// TODO Auto-generated method stub
		super.processComponentEvent(e);

		if (e.getID() == ComponentEvent.COMPONENT_RESIZED) {

		}
	}

	public Movie getMovie() {
		builder.setTitle(title.getText());
		builder.setSynopsis(synopsis.getText());
		builder.setVenue(venue.getText());
		builder.setExhibitions(new ArrayList<>(exhibitions.getData()));
		builder.setPosterUrl(posterUrl.getText());

		String tags[] = this.tags.getText().split(",");
		for (int i = 0; i < tags.length; i++) {
			tags[i] = tags[i].trim();
		}

		builder.setTags(new ArrayList<String>(Arrays.asList(tags)));
		builder.setIMDBLink(IMDBLink.getText());

		return builder.create();
	}

	public void setMovie(Movie m) {
		builder.copyFrom(m);
		updateComponents();
	}

	public void setMovie(MovieBuilder mb) {
		this.builder.copyFrom(mb);
		updateComponents();
	}

	private void updateComponents() {
		title.setText(builder.getTitle());
		synopsis.setText(builder.getSynopsis());
		venue.setText(builder.getVenue());
		exhibitions.setData(builder.getExhibitions());
		posterUrl.setText(builder.getPosterUrl());
		IMDBLink.setText(builder.getIMDBLink());
		tags.setText(StringUtils.join(builder.getTags(), ", "));
		genres.setText(StringUtils.join(builder.getGenres(), ", "));
		ge.listChanged(builder.getGenres());
	}
}
