package org.kinema.ui.panels;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import net.miginfocom.swing.MigLayout;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.kinema.model.Movie;
import org.kinema.ui.ImagePanel;

public class MovieDisplay extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Movie movie;

	private JTextPane title;
	private JTextPane synopsis;
	private JTextPane exhibitions;
	private JTextPane venue;
	private ImagePanel poster = null;
	private JTextPane genres;
	private JTextPane tags;
	private JEditorPane IMDBLink;

	private JEditorPane data;

	public MovieDisplay(Movie m) {
		MigLayout layout = new MigLayout("wrap, gapy 0!, fill");
		setLayout(layout);

		poster = new ImagePanel();
		poster.setImageSize(110, ImagePanel.SIZE_PROPORTIONAL);
		add(poster, "dock west, w 110!, gapright 6");

		Font f = UIManager.getDefaults().getFont("Label.font");

		title = new JTextPane();
		title.setEditable(false);
		title.setBackground(null);
		title.setOpaque(false);
		title.setFont(f.deriveFont(Font.BOLD, 14.0f));
		// title.setContentType("text/html");
		add(title);
		// add(new JSeparator());

		genres = new JTextPane();
		genres.setEditable(false);
		genres.setBackground(null);
		genres.setOpaque(false);
		genres.setFont(f.deriveFont(Font.ITALIC));
		genres.setForeground(Color.GRAY);
		// genres.setContentType("text/html");
		add(genres, "gapleft 10");

		synopsis = new JTextPane();
		synopsis.setEditable(false);
		synopsis.setBackground(null);
		synopsis.setOpaque(false);
		// synopsis.setContentType("text/html");
		add(synopsis);

		venue = new JTextPane();
		venue.setEditable(false);
		venue.setBackground(null);
		venue.setOpaque(false);
		// venue.setContentType("text/html");
		venue.setToolTipText("Where the movie will be played.");
		JLabel vl = new JLabel("Where");
		vl.setFont(f.deriveFont(Font.BOLD));
		add(vl, "split 2");
		add(venue);

		exhibitions = new JTextPane();
		exhibitions.setEditable(false);
		exhibitions.setBackground(null);
		exhibitions.setOpaque(false);
		exhibitions.setContentType("text/html");
		JLabel ve = new JLabel("When");
		ve.setFont(f.deriveFont(Font.BOLD));
		add(ve, "split 2, aligny top, gaptop 3");
		add(exhibitions);

		tags = new JTextPane();
		tags.setEditable(false);
		tags.setBackground(null);
		tags.setOpaque(false);
		// tags.setContentType("text/html");
		tags.setToolTipText("A comma separated list of tags");
		JLabel vt = new JLabel("Tags");
		vt.setFont(f.deriveFont(Font.BOLD));
		add(vt, "split 2");
		add(tags);

		IMDBLink = new JEditorPane();
		IMDBLink.setContentType("text/html");
		IMDBLink.setOpaque(false);
		IMDBLink.setEditable(false);
		IMDBLink.addHyperlinkListener(new HyperlinkListener() {
			@Override
			public void hyperlinkUpdate(HyperlinkEvent hle) {
				if (HyperlinkEvent.EventType.ACTIVATED.equals(hle
						.getEventType())) {
					System.out.println(hle.getURL());
					Desktop desktop = Desktop.getDesktop();
					try {
						URL url = hle.getURL();
						if (url != null)
							desktop.browse(url.toURI());
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		});
		IMDBLink.setToolTipText("A link to the IMDB page for the movie if any");
		add(IMDBLink, "ay top");

		data = new JEditorPane();
		data.setContentType("text/html");
		data.setOpaque(false);
		data.setEditable(false);
		data.addHyperlinkListener(new HyperlinkListener() {
			@Override
			public void hyperlinkUpdate(HyperlinkEvent hle) {
				if (HyperlinkEvent.EventType.ACTIVATED.equals(hle
						.getEventType())) {
					System.out.println(hle.getURL());
					Desktop desktop = Desktop.getDesktop();
					try {
						URL url = hle.getURL();
						if (url != null)
							desktop.browse(url.toURI());
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		});
		// add(data);

		setMovie(m);
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;

		if (movie != null) {
			try {
				poster.setFromUrl(new URL(movie.getPosterUrl()));
			} catch (MalformedURLException e) {
			} catch (IOException e) {
			}

			title.setText(movie.getTitle());
			synopsis.setText(movie.getSynopsis());
			venue.setText(movie.getVenue());
			exhibitions.setText(buildExhibitionList());
			genres.setText(StringUtils.join(movie.getGenres(), ", "));
			tags.setText(StringUtils.join(movie.getTags(), ", "));

			String lnk = movie.getIMDBLink();
			if (!lnk.matches("https?://.*")) {
				try {
					lnk = "http://" + URLEncoder.encode(lnk, "UTF-8");
				} catch (UnsupportedEncodingException e) {
				}
			}
			IMDBLink.setText("<html><a href=\"" + lnk + "\">"
					+ movie.getIMDBLink() + "</a></html>");
		}

		revalidate();
	}

	public String buildExhibitionList() {
		ArrayList<Date> dates = new ArrayList<>(movie.getExhibitions());
		StringBuilder b = new StringBuilder("<html>");

		Collections.sort(dates);

		Date day = new Date(0);
		DateFormat fmt = new SimpleDateFormat("HH:mm");
		DateFormat dayFmt = new SimpleDateFormat("dd/MM/yyyy");

		boolean first = true;

		for (int i = 0; i < dates.size(); i++) {
			Date dat = dates.get(i);
			if (DateUtils.isSameDay(day, dat)) {
				b.append(" " + fmt.format(dat));
			} else {
				day = dat;
				if (!first)
					b.append("<br/>");
				else
					first = false;
				b.append("<b>" + dayFmt.format(dat) + "</b>");
				b.append(" " + fmt.format(dat));
			}
		}

		b.append("</html>");

		return b.toString();
	}

}
