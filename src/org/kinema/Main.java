package org.kinema;

import javax.swing.SwingUtilities;

public class Main {

	public static void main(final String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new KApplication(args);
			}
		});
	}

}
