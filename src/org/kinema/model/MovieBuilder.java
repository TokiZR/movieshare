package org.kinema.model;

import java.util.ArrayList;
import java.util.Date;

public class MovieBuilder {

	private String title;
	private String synopsis;
	private ArrayList<Date> exhibitions;
	private String venue;
	private String posterUrl;
	private ArrayList<Genre> genres;
	private ArrayList<String> tags;
	private String IMDBLink;

	public MovieBuilder() {
		title = "";
		synopsis = "";
		exhibitions = new ArrayList<Date>();
		venue = "";
		posterUrl = "";
		genres = new ArrayList<Genre>();
		tags = new ArrayList<String>();
		IMDBLink = "";
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSynopsis() {
		return synopsis;
	}

	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}

	public ArrayList<Date> getExhibitions() {
		return exhibitions;
	}

	public void setExhibitions(ArrayList<Date> exhibitions) {
		this.exhibitions = exhibitions;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public String getPosterUrl() {
		return posterUrl;
	}

	public void setPosterUrl(String posterUrl) {
		this.posterUrl = posterUrl.trim();
	}

	public ArrayList<Genre> getGenres() {
		return genres;
	}

	public void setGenres(ArrayList<Genre> genres) {
		this.genres = genres;
	}

	public ArrayList<String> getTags() {
		return tags;
	}

	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}

	public String getIMDBLink() {
		return IMDBLink.trim();
	}

	public void setIMDBLink(String iMDBLink) {
		IMDBLink = iMDBLink;
	}

	public Movie create() {
		return new Movie(title, synopsis, exhibitions, venue, posterUrl, genres, tags, IMDBLink);
	}

	public void copyFrom(Movie m) {
		this.title = m.getTitle();
		this.synopsis = m.getSynopsis();
		this.exhibitions = new ArrayList<Date>(m.getExhibitions());
		this.venue = m.getVenue();
		this.posterUrl = m.getPosterUrl();
		this.genres = new ArrayList<Genre>(m.getGenres());
		this.tags = new ArrayList<String>(m.getTags());
		this.IMDBLink = m.getIMDBLink();
	}

	public void copyFrom(MovieBuilder mb) {
		this.title = mb.title;
		this.synopsis = mb.synopsis;
		this.exhibitions = new ArrayList<Date>(mb.exhibitions);
		this.venue = mb.venue;
		this.posterUrl = mb.posterUrl;
		this.genres = new ArrayList<Genre>(mb.genres);
		this.tags = new ArrayList<String>(mb.tags);
		this.IMDBLink = mb.IMDBLink;
	}
}
