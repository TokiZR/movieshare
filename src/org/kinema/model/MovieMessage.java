package org.kinema.model;

import rice.p2p.commonapi.Message;

public class MovieMessage implements Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6766482512103449971L;

	public Movie movie;

	public MovieMessage() {
		movie = null;
	}

	public MovieMessage(Movie m) {
		movie = m;
	}

	@Override
	public int getPriority() {
		return Message.MEDIUM_PRIORITY;
	}
	
	@Override
	public String toString() {
		return movie.toString();
	}
}
