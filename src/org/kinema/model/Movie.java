package org.kinema.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public final class Movie implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8846409834364793172L;
	
	private String title;
	private String synopsis;
	private ImmutableList<Date> exhibitions;
	private String venue;
	private String posterUrl;
	private ImmutableList<Genre> genres;
	private ImmutableList<String> tags;
	private String IMDBLink;

	public Movie(String title, String synopsis, ArrayList<Date> exibitions,
			String venue, String posterUrl, ArrayList<Genre> genres,
			ArrayList<String> tags, String IMDBLink) {
		this.title = title;
		this.synopsis = synopsis;
		this.exhibitions = ImmutableList.copyOf(exibitions);
		this.venue = venue;
		this.posterUrl = posterUrl;
		this.genres = ImmutableList.copyOf(genres);
		this.tags = ImmutableList.copyOf(tags);
		this.IMDBLink = IMDBLink;
	}

	public final String getTitle() {
		return title;
	}

	public final String getSynopsis() {
		return synopsis;
	}

	public final ImmutableList<Date> getExhibitions() {
		return exhibitions;
	}

	public final String getVenue() {
		return venue;
	}

	public final String getPosterUrl() {
		return posterUrl;
	}

	public final ImmutableList<Genre> getGenres() {
		return genres;
	}

	public final ImmutableList<String> getTags() {
		return tags;
	}

	public final String getIMDBLink() {
		return IMDBLink;
	}

	@Override
	public String toString() {
		Gson g = new GsonBuilder().setPrettyPrinting().create();
		return g.toJson(this);
	}
}
