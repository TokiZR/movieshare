package org.kinema.model;

public enum Genre {
	Action,
	Drama,
	Adventure,
	Animation,
	Comedy,
	Sci_Fi,
	Crime,
	Thriller,
	Fantasy,
	Mystery,
	Family,
	Romance,
	Sport,
	Horror,
	War,
	Western,
	Reality_TV,
	History,
	Documentary,
	GameShow,
	Music,
	Biography,
	Musical,
	News,
	TalkShow,
	Film_Noir,
	Adult;
	
	@Override
	public String toString () {
		switch(this) {
			case Action:
				return "Action";
			case Drama:
				return "Drama";
			case Adventure:
				return "Adventure";
			case Animation:
				return "Animation";
			case Comedy:
				return "Comedy";
			case Sci_Fi:
				return "Sci-Fi";
			case Crime:
				return "Crime";
			case Thriller:
				return "Thriller";
			case Fantasy:
				return "Fantasy";
			case Mystery:
				return "Mystery";
			case Family:
				return "Family";
			case Romance:
				return "Romance";
			case Sport:
				return "Sport";
			case Horror:
				return "Horror";
			case War:
				return "War";
			case Western:
				return "Western";
			case Reality_TV:
				return "Reality-TV";
			case History:
				return "History";
			case Documentary:
				return "Documentary";
			case GameShow:
				return "Game Show";
			case Music:
				return "Music";
			case Biography:
				return "Biography";
			case Musical:
				return "Musical";
			case News:
				return "New";
			case TalkShow:
				return "Talk Show";
			case Film_Noir:
				return "Film-Noir";
			case Adult:
				return "Adult";
			default:
				return "null";
		}
	}
}