package org.kinema.model;

import rice.p2p.commonapi.Message;

public class StringMessage implements Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4970069775902550712L;
	private String content;
	
	public StringMessage(String msg) {
		content = msg;
	}

	@Override
	public int getPriority() {
		return 0;
	}
	
	public String getContent() {
		return content;
	}
	
	@Override
	public String toString() {
		return content;
	}
}
