package org.kinema.model;

import rice.p2p.commonapi.Message;
import rice.p2p.scribe.ScribeContent;

public class ScribeMessageContent implements ScribeContent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5930885369857752198L;
	
	private Message message;

	public ScribeMessageContent(Message m) {
		message = m;
	}
	
	public Message getMessage() {
		return message;
	}
	
	@Override
	public String toString() {
		return message.toString();
	}
}
