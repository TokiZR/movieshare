package org.kinema.model;

import java.util.ArrayList;
import java.util.Arrays;

public class Subscriptions {

	public ArrayList<Genre> genres;
	public ArrayList<String> tags;

	public Subscriptions() {
		genres = new ArrayList<>(Arrays.asList(Genre.values()));
		tags = new ArrayList<>();
	}

	public boolean match(Movie m) {
		for (Genre g : genres) {
			for (Genre mg : m.getGenres()) {
				if (g == mg)
					return true;
			}
		}

		for (String t : tags) {
			for (String mt : m.getTags()) {
				if (t.equals(mt))
					return true;
			}
		}

		return false;
	}

}
