package org.kinema;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import net.miginfocom.swing.MigLayout;

import org.kinema.model.Genre;
import org.kinema.model.Movie;
import org.kinema.model.MovieBuilder;
import org.kinema.model.Subscriptions;
import org.kinema.ui.DateListCellEditorRenderer;
import org.kinema.ui.EditableList;
import org.kinema.ui.panels.MovieDisplay;
import org.kinema.ui.panels.MovieEditor;
import org.kinema.util.NetUtils;

import rice.environment.Environment;
import rice.tutorial.lesson4.DistTutorial;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Test {

	public Test() {
		// TODO Auto-generated constructor stub
	}

	public static void setSystemLF() {
		try {
			// Set System L&F
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			// UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (UnsupportedLookAndFeelException e) {
		} catch (ClassNotFoundException e) {
		} catch (InstantiationException e) {
		} catch (IllegalAccessException e) {
		}
	}

	public static void testMatch() {
		MovieBuilder mb = new MovieBuilder();
		mb.setTitle("Awesome movie");
		mb.getGenres().add(Genre.Action);
		mb.getGenres().add(Genre.Comedy);
		mb.getGenres().add(Genre.Drama);
		mb.getGenres().add(Genre.Fantasy);
		mb.getGenres().add(Genre.Romance);
		mb.getGenres().add(Genre.Family);
		mb.getTags().add("Awesome");

		Movie a = mb.create();

		mb = new MovieBuilder();
		mb.setTitle("Spider Pig");
		mb.getGenres().add(Genre.Action);
		mb.getGenres().add(Genre.Crime);
		mb.getTags().add("Lame");

		Movie b = mb.create();

		Subscriptions sbs = new Subscriptions();
		sbs.genres.add(Genre.Drama);

		System.out.println("Movie a matches? " + (sbs.match(a) ? "yes" : "no"));
		System.out.println("Movie b matches? " + (sbs.match(b) ? "yes" : "no"));
	}

	public static void testSerialize() {
		MovieBuilder mb = new MovieBuilder();
		mb.setTitle("Awesome movie");
		mb.getGenres().add(Genre.Action);
		mb.getGenres().add(Genre.Comedy);
		mb.getGenres().add(Genre.Drama);
		mb.getGenres().add(Genre.Fantasy);
		mb.getGenres().add(Genre.Romance);
		mb.getGenres().add(Genre.Family);
		mb.getGenres().add(Genre.Film_Noir);
		mb.getTags().add("Awesome");

		Movie m = mb.create();

		Gson g = new GsonBuilder().setPrettyPrinting().create();

		String mJ = g.toJson(m);

		System.out.println(mJ);

		Movie m2 = ((MovieBuilder) g.fromJson(mJ, MovieBuilder.class)).create();

		System.out.println(m2.getTitle());
		System.out.println(m2.getGenres());
	}

	public static void testMovieUI() {
		final Gson g = new GsonBuilder().setPrettyPrinting().create();

		setSystemLF();

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				final MovieEditor me = new MovieEditor();
				final JFrame frame = new JFrame();
				final MovieDisplay display = new MovieDisplay(null);
				frame.setLayout(new MigLayout());

				JButton done = new JButton("Done");
				done.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent paramActionEvent) {
						Movie m = me.getMovie();
						System.out.println(g.toJson(m));
						display.setMovie(m);
					}
				});
				frame.add(done, "wrap");

				String movie = "{   \"title\": \"Transformers: Age of Extinction\",   \"synopsis\": \"An automobile mechanic and his daughter make a discovery that brings down the Autobots - and a paranoid government official - on them.\",   \"exhibitions\": [     \"Jun 29, 2014 8:00:00 PM\",     \"Jul 2, 2014 7:30:00 PM\",     \"Jul 2, 2014 10:45:00 PM\"   ],   \"venue\": \"My House\",   \"posterUrl\": \"http://ia.media-imdb.com/images/M/MV5BMjEwNTg1MTA5Nl5BMl5BanBnXkFtZTgwOTg2OTM4MTE@._V1_SX214_AL_.jpg\",   \"genres\": [     \"Action\",     \"Adventure\",     \"Sci_Fi\"   ],   \"tags\": [     \"transformers\",     \"dinosaurs\",     \"silly storyline\"   ],   \"IMDBLink\": \"http://www.imdb.com/title/tt2109248/?ref_\u003dnv_sr_1\" }";

				MovieBuilder m = g.fromJson(movie, MovieBuilder.class);

				me.setMovie(m);
				display.setMovie(m.create());

				frame.add(me);
				frame.add(display);
				frame.pack();
				frame.setVisible(true);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			}
		});
	}

	public static void testListEditor() {
		setSystemLF();

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				final JFrame frame = new JFrame();

				EditableList<Date> e = new EditableList<>(Date.class);

				e.setCellEditor(new DateListCellEditorRenderer());

				frame.add(e);
				frame.pack();
				frame.setVisible(true);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			}
		});
	}

	public static void testPastry() {
		final Environment env = new Environment();

		// disable the UPnP setting (in case you are testing this on a NATted
		// LAN)
		env.getParameters().setString("nat_search_policy", "never");

		setSystemLF();

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				final JFrame frame = new JFrame();
				frame.setLayout(new MigLayout("wrap", "[80, align right][]"));

				final JTextField port, ip, lport;

				ip = new JTextField();
				frame.add(new JLabel("Ring IP"));
				ip.setToolTipText("ip");
				frame.add(ip, "w 180!");

				port = new JTextField();
				frame.add(new JLabel("Ring Port"));
				port.setToolTipText("port");
				frame.add(port, "w 100!");

				lport = new JTextField();
				frame.add(new JLabel("Bind Port"));
				lport.setToolTipText("lport");
				frame.add(lport, "w 100!");

				JButton start = new JButton("Start");
				start.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent paramActionEvent) {
						InetSocketAddress bootaddress;
						int bindport;

						bindport = Integer.parseInt(lport.getText());
						bootaddress = new InetSocketAddress(ip.getText(), Integer.parseInt(port
								.getText()));

						try {
							@SuppressWarnings("unused")
							DistTutorial dt = new DistTutorial(bindport, bootaddress, 1, env);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
				frame.add(start, "skip");

				frame.pack();
				frame.setVisible(true);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			}
		});
	}

	public static void main(String[] args) throws UnknownHostException, SocketException {
		List<InetAddress> addrs = NetUtils.getLocalAddresses();

		for (InetAddress addr : addrs) {
			System.out.println(addr.getCanonicalHostName());
		}
	}
}
