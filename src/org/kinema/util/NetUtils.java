package org.kinema.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;

public class NetUtils {

	/**
	 * Timeout for the cached current IP in minutes.
	 */
	static private final int addressTimeout = 5;

	static private InetAddress externalAddress = null;

	static private Date expire;

	public static InetAddress getExternalIp(boolean forceRefresh) {

		if (externalAddress != null && new Date().before(expire)
				&& !forceRefresh) {
			return externalAddress;
		} else {
			String remote = "http://checkip.amazonaws.com/";
			try {
				URL connection = new URL(remote);
				URLConnection con = connection.openConnection();
				String str = null;
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(con.getInputStream()));
				str = reader.readLine();
				externalAddress = InetAddress.getByName(str);

				expire = DateUtils.addMinutes(new Date(), addressTimeout);

				return externalAddress;
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
	}

	public static List<InetAddress> getLocalAddresses() {
		ArrayList<InetAddress> addresses = new ArrayList<>();

		Enumeration<?> e;
		try {
			e = NetworkInterface.getNetworkInterfaces();
			while (e.hasMoreElements()) {
				NetworkInterface n = (NetworkInterface) e.nextElement();
				Enumeration<?> ee = n.getInetAddresses();
				while (ee.hasMoreElements()) {
					InetAddress i = (InetAddress) ee.nextElement();
					if (!i.isMulticastAddress())
						addresses.add(i);
				}
			}
		} catch (SocketException ex) {
			ex.printStackTrace();
		}

		InetAddress external = getExternalIp(false);
		if (external != null && !addresses.contains(external))
			addresses.add(external);

		return addresses;
	}

	private static String hostName(InetAddress addr) {
		if (addr.getCanonicalHostName().equals(addr.getHostAddress()))
			return addr.getHostAddress();
		else
			return addr.getCanonicalHostName() + "(" + addr.getHostAddress()
					+ ")";
	}

	public static List<String> getLocalAddressNames() {
		ArrayList<String> addresses = new ArrayList<>();

		Enumeration<?> e;
		try {
			e = NetworkInterface.getNetworkInterfaces();
			while (e.hasMoreElements()) {
				NetworkInterface n = (NetworkInterface) e.nextElement();
				Enumeration<?> ee = n.getInetAddresses();
				while (ee.hasMoreElements()) {
					InetAddress i = (InetAddress) ee.nextElement();
					if (!i.isMulticastAddress())
						addresses.add(hostName(i));
				}
			}
		} catch (SocketException ex) {
			ex.printStackTrace();
		}

		InetAddress external = getExternalIp(false);
		if (external != null && !addresses.contains(external))
			addresses.add(hostName(external));

		return addresses;
	}

}
