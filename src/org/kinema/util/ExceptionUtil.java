package org.kinema.util;

public class ExceptionUtil {
	public static Throwable getRootCause(Throwable t) {
		while (t != null && t.getCause() != null && t.getCause() != t)
			t = t.getCause();

		return t;
	}
}
