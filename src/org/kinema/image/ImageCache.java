package org.kinema.image;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.imageio.ImageIO;
import javax.swing.SwingWorker;

public class ImageCache {
	private static ImageCache defaultIC = new ImageCache();
	
	private HashMap<URL, BufferedImage> imageMap;

	public ImageCache() {
		imageMap = new HashMap<>();
	}

	public void getImage(URL locator, ImageLoadedListener list)
			throws IOException {
		if (imageMap.containsKey(locator)) {
			list.imageLoaded(imageMap.get(locator));
		} else {
			new ImageLoader(locator, list).execute();
		}
	}
	
	public static ImageCache getDefault() {
		return defaultIC;
	}

	private class ImageLoader extends SwingWorker<BufferedImage, IOException> {

		private URL locator;
		private ImageLoadedListener listener;

		public ImageLoader(URL locator, ImageLoadedListener listener) {
			this.locator = locator;
			this.listener = listener;
		}

		@Override
		protected BufferedImage doInBackground() throws Exception {
			BufferedImage image = null;
			try {
				image = ImageIO.read(locator);
			} catch (IOException e) {
				publish(e);
			}
			if (image != null)
				imageMap.put(locator, image);

			return image;
		}

		@Override
		protected void process(List<IOException> chunks) {
			for(IOException e : chunks) {
				listener.exception(e);
			}
		}
		
		@Override
		protected void done() {
			try {
				listener.imageLoaded(get());
			} catch (InterruptedException e) {
				listener.exception(e);
			} catch (ExecutionException e) {
				listener.exception(e);
			}
		}
	}
}
