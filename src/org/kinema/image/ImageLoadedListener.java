package org.kinema.image;

import java.awt.image.BufferedImage;

public interface ImageLoadedListener {

	public void imageLoaded(BufferedImage im);
	
	public void exception(Exception e);
	
}
