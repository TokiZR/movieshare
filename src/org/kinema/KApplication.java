package org.kinema;

import gnu.getopt.Getopt;
import gnu.getopt.LongOpt;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.EventListenerList;

import org.apache.commons.io.IOUtils;
import org.kinema.model.Movie;
import org.kinema.model.MovieBuilder;
import org.kinema.model.MovieMessage;
import org.kinema.model.Subscriptions;
import org.kinema.ui.MainWindow;
import org.kinema.util.ExceptionUtil;

import com.google.common.net.HostAndPort;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;

public class KApplication {

	protected EventListenerList listenerList = new EventListenerList();

	private MainWindow mainWindow;

	private DefaultListModel<Movie> movies;

	private PastryApplication pastry = null;

	private Settings settings;

	private String connStatus;

	private boolean noSettings = false;

	private Logger logger;

	private static void setSystemLF() {
		try {
			if (UIManager.getSystemLookAndFeelClassName().equals(
					"javax.swing.plaf.metal.MetalLookAndFeel"))
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
			else
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			// UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (UnsupportedLookAndFeelException e) {
		} catch (ClassNotFoundException e) {
		} catch (InstantiationException e) {
		} catch (IllegalAccessException e) {
		}
	}

	public KApplication(String[] args) {
		setSystemLF();

		/******
		 * Parse command line arguments.
		 */
		String settingsPath = null;
		String connectOverride = null;
		String localHost = null;
		boolean transformers = false;
		boolean boot = false;
		boolean connect = false;

		LongOpt opts[] = new LongOpt[] { new LongOpt("settings", LongOpt.NO_ARGUMENT, null, 's'),
				new LongOpt("transient", LongOpt.NO_ARGUMENT, null, 't'),
				new LongOpt("transformers", LongOpt.NO_ARGUMENT, null, 'x'),
				new LongOpt("connect", LongOpt.OPTIONAL_ARGUMENT, null, 'c'),
				new LongOpt("local", LongOpt.REQUIRED_ARGUMENT, null, 'l'),
				new LongOpt("boot", LongOpt.NO_ARGUMENT, null, 'b') };

		Getopt g = new Getopt("Kinema", args, "stbc::l:x", opts);
		g.setOpterr(true);
		int c;

		while ((c = g.getopt()) != -1) {
			switch (c) {
			case 's':
				settingsPath = g.getOptarg();
				break;
			case 't':
				noSettings = true;
				break;
			case 'b':
				boot = true;
				break;
			case 'c':
				connectOverride = g.getOptarg();
				connect = true;
				break;
			case 'l':
				localHost = g.getOptarg();
				break;
			case 'x':
				transformers = true;
				break;
			case '?':
				System.exit(1);
				break;
			}
		}

		if (boot && connect) {
			System.err
					.println("Both connect and boot arguments provided, use only either ot them.");
			System.exit(1);
		}

		logger = Logger.getGlobal();
		logger.setLevel(Level.ALL);

		/*****
		 * Setup
		 */
		try {
			settings = Settings.load(settingsPath);
		} catch (IOException e) {
			System.err
					.println("Cannot open settings location, the current session data will probably not be saved.");
			settings = new Settings();
		}

		connStatus = "Disconnected";

		mainWindow = new MainWindow(this);
		mainWindow.addWindowListener(windowListener);
		movies = (DefaultListModel<Movie>) mainWindow.getMovies();

		mainWindow.setVisible(true);

		if (localHost != null) {
			HostAndPort hp = HostAndPort.fromString(localHost).requireBracketsForIPv6();
			if (hp.getHostText() != null && !hp.getHostText().equals("")) {
				settings.setLocalHost(hp.getHostText());
				System.out.println("Local host set to: " + hp.getHostText());
			}
			if (hp.getPort() >= 0) {
				settings.setLocalPort(hp.getPort());
				System.out.println("Local port set to: " + hp.getPort());
			}
		}

		if (connectOverride != null) {
			HostAndPort hp = HostAndPort.fromString(connectOverride).withDefaultPort(6464)
					.requireBracketsForIPv6();
			settings.setRemoteHost(hp.getHostText());
			settings.setRemotePort(hp.getPort());
			System.out.println("Remote host set to: " + hp);
		}

		if (settings.getAutoBoot() || boot) {
			try {
				bootPastry();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(mainWindow,
						"Cannot boot node: " + e.getLocalizedMessage(), "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		} else if (settings.getAutoConnect() || connect) {
			try {
				connectPastry();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(mainWindow,
						"Cannot connect to remote host: " + e.getLocalizedMessage(), "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}

		try {
			loadMovies();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(mainWindow,
					"Failed to load stored movies: " + e.getLocalizedMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
		}

		if (transformers) {
			String movie = "{   \"title\": \"Transformers: Age of Extinction\",   \"synopsis\": \"An automobile mechanic and his daughter make a discovery that brings down the Autobots - and a paranoid government official - on them.\",   \"exhibitions\": [     \"Jun 29, 2014 8:00:00 PM\",     \"Jul 2, 2014 7:30:00 PM\",     \"Jul 2, 2014 10:45:00 PM\"   ],   \"venue\": \"My House\",   \"posterUrl\": \"http://ia.media-imdb.com/images/M/MV5BMjEwNTg1MTA5Nl5BMl5BanBnXkFtZTgwOTg2OTM4MTE@._V1_SX214_AL_.jpg\",   \"genres\": [     \"Action\",     \"Adventure\",     \"Sci_Fi\"   ],   \"tags\": [     \"transformers\",     \"dinosaurs\",     \"silly storyline\"   ],   \"IMDBLink\": \"http://www.imdb.com/title/tt2109248/?ref_\u003dnv_sr_1\" }";

			if (pastry != null)
				publishMovie(new Gson().fromJson(movie, MovieBuilder.class).create());
			else
				movieArrived(new Gson().fromJson(movie, MovieBuilder.class).create());
		}
	}

	WindowListener windowListener = new WindowAdapter() {
		@Override
		public void windowClosing(WindowEvent e) {
			quit();
		}
	};

	public MainWindow getMainWindow() {
		return mainWindow;
	}

	public Settings getSettings() {
		return settings;
	}

	public PastryApplication getPastry() {
		return pastry;
	}

	public void addStatusEventListener(StatusEventListener listener) {
		listenerList.add(StatusEventListener.class, listener);
	}

	public void removeStatusEventListener(StatusEventListener listener) {
		listenerList.remove(StatusEventListener.class, listener);
	}

	void fireStatusEvent(StatusEvent evt) {
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i + 2) {
			if (listeners[i] == StatusEventListener.class) {
				((StatusEventListener) listeners[i + 1]).onStatusChange(evt);
			}
		}
	}

	public void movieArrived(Movie movie) {
		if (settings.getSubs().match(movie)) {
			movies.add(movies.getSize(), movie);
		}
	}

	public void connectPastry() throws Exception {
		if (pastry != null) {
			pastry.disconnect();
		}

		try {
			pastry = new PastryApplication(this, settings.getRemoteHost(),
					settings.getRemotePort(), settings.getLocalPort());
			connStatus = "<html><span style=\"color: green\">" + "Connected to "
					+ settings.getRemoteHost() + ":" + settings.getRemotePort() + "</span></html>";
			if (settings.getLocalPort() == 0)
				settings.setLocalPort(pastry.getPort());
			fireStatusEvent(new StatusEvent(this));
		} catch (Exception e) {
			connStatus = "<html><span style=\"color: red\">" + "Cannot connect to remote host: "
					+ ExceptionUtil.getRootCause(e).getLocalizedMessage() + "</span></html>";
			fireStatusEvent(new StatusEvent(this));
			logger.warning(e.toString());
			logger.throwing(getClass().getName(),
					Thread.currentThread().getStackTrace()[0].getMethodName(), e);
			throw (Exception) ExceptionUtil.getRootCause(e);
		}
	}

	public void bootPastry() throws Exception {
		if (pastry != null) {
			pastry.disconnect();
		}

		try {
			pastry = new PastryApplication(this, settings.getLocalPort());
			connStatus = "<html><span style=\"color: green\">" + "Booted to local ring on port "
					+ settings.getLocalPort() + "</span></html>";
			if (settings.getLocalPort() == 0)
				settings.setLocalPort(pastry.getPort());
			fireStatusEvent(new StatusEvent(this));
		} catch (Exception e) {
			connStatus = "<html><span style=\"color: red\">" + "Cannot boot node: "
					+ ExceptionUtil.getRootCause(e).getLocalizedMessage() + "</span></html>";
			fireStatusEvent(new StatusEvent(this));
			logger.warning(e.toString());
			logger.throwing(getClass().getName(),
					Thread.currentThread().getStackTrace()[0].getMethodName(), e);
			throw (Exception) ExceptionUtil.getRootCause(e);
		}
	}

	public String getConnStatus() {
		return connStatus;
	}

	public void quit() {
		if (pastry != null)
			pastry.disconnect();

		if (!noSettings) {
			try {
				saveMovies();

				settings.save();
			} catch (IOException e) {
				System.err.println("Could not save the current settings.");
			}
		}

		System.exit(0);
	}

	public void saveMovies() throws IOException {
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.setPrettyPrinting().create();

		File f = new File(settings.getSettingsPath(), "kinema.movies.json");
		f.getParentFile().mkdirs();
		f.createNewFile();

		FileOutputStream fos = new FileOutputStream(f);

		JsonArray array = new JsonArray();

		for (int i = 0; i < movies.getSize(); i++) {
			array.add(gson.toJsonTree(movies.get(i)));
		}

		fos.write(gson.toJson(array).getBytes());

		fos.close();
	}

	public void loadMovies() throws IOException {
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.setPrettyPrinting().create();

		File f = new File(settings.getSettingsPath(), "kinema.movies.json");
		if (f.exists()) {
			FileInputStream fis = new FileInputStream(f);

			String data = IOUtils.toString(fis);

			fis.close();

			MovieBuilder movies[] = gson.fromJson(data, MovieBuilder[].class);

			for (MovieBuilder m : movies) {
				this.movies.add(this.movies.getSize(), m.create());
			}
		}
	}

	public Subscriptions getSubscriptions() {
		return settings.getSubs();
	}

	public void publishMovie(Movie movie) {
		pastry.publishMessage(new MovieMessage(movie));
		// pastry.publishMessage(new StringMessage("Boooo"));
	}
}