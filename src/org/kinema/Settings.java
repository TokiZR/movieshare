package org.kinema;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;

import org.apache.commons.io.IOUtils;
import org.kinema.model.Subscriptions;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Settings implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 22131231L;

	private static final String defaultSettingsPath = "/.config/kinema/";

	private static final String settingsFile = "kinema.settings.json";

	private transient String settingsPath = "";

	private Subscriptions subs;

	private String remoteHost;

	private int remotePort;

	private String localHost;

	private int localPort;

	private boolean autoBoot;

	private boolean autoConnect;

	public Settings() {
		File settingsFolder = new File(System.getProperty("user.home"));

		subs = new Subscriptions();

		remoteHost = "localhost";
		remotePort = PastryApplication.DEFAULT_PORT;
		localHost = "localhost";
		localPort = PastryApplication.DEFAULT_PORT;

		settingsPath = new File(settingsFolder, defaultSettingsPath).getPath();
	}

	/**
	 * Save the settings to the current settings path.
	 */
	public void save() throws IOException {
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.setPrettyPrinting().create();

		File f = new File(settingsPath, settingsFile);
		f.getParentFile().mkdirs();
		f.createNewFile();

		FileOutputStream fos = new FileOutputStream(f);

		fos.write(gson.toJson(this).getBytes());

		fos.close();
	}

	/**
	 * Loads settings from a file.
	 * 
	 * If the file does not exist, if reading fails for some reason an exception
	 * is thrown.
	 * 
	 * If the path is null then the default path is used.
	 * 
	 * 
	 * @param path
	 *            The file path.
	 * @return The read settings or null.
	 * 
	 * @throws IOException
	 */
	public static Settings load(String path) throws IOException {
		Settings s = null;

		File f;
		if (path == null) {
			f = new File(System.getProperty("user.home"), defaultSettingsPath + settingsFile);
		} else {
			f = new File(path);
		}

		if (f.exists()) {
			FileInputStream fis = new FileInputStream(f);

			String data = IOUtils.toString(fis);

			fis.close();

			GsonBuilder gb = new GsonBuilder();

			Gson gson = gb.setPrettyPrinting().create();

			s = gson.fromJson(data, Settings.class);

		} else {
			s = new Settings();
		}

		s.settingsPath = f.getParent();

		return s;
	}

	public String getSettingsPath() {
		return settingsPath;
	}

	public void setSettingsPath(String settingsPath) {
		if (settingsPath != null)
			this.settingsPath = settingsPath;
		else
			this.settingsPath = new File(System.getProperty("user.home"), defaultSettingsPath)
					.getPath();
	}

	public Subscriptions getSubs() {
		return subs;
	}

	public void setSubs(Subscriptions subs) {
		this.subs = subs;
	}

	public String getRemoteHost() {
		return remoteHost;
	}

	public void setRemoteHost(String remoteHost) {
		this.remoteHost = remoteHost;
	}

	public int getRemotePort() {
		return remotePort;
	}

	public void setRemotePort(int remotePort) {
		this.remotePort = remotePort;
	}

	public String getLocalHost() {
		return localHost;
	}

	public void setLocalHost(String localHost) {
		this.localHost = localHost;
	}

	public int getLocalPort() {
		return localPort;
	}

	public void setLocalPort(int localPort) {
		this.localPort = localPort;
	}

	public boolean getAutoBoot() {
		return autoBoot;
	}

	public void setAutoBoot(boolean autoBoot) {
		if (autoBoot && autoConnect) {
			autoConnect = false;
		}
		this.autoBoot = autoBoot;
	}

	public boolean getAutoConnect() {
		return autoConnect;
	}

	public void setAutoConnect(boolean autoConnect) {
		if (autoBoot && autoConnect) {
			autoBoot = false;
		}
		this.autoConnect = autoConnect;
	}
}