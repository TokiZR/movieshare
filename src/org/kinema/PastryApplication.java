package org.kinema;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.Collection;

import org.kinema.model.MovieMessage;
import org.kinema.model.ScribeMessageContent;
import org.kinema.model.StringMessage;

import rice.environment.Environment;
import rice.p2p.commonapi.Application;
import rice.p2p.commonapi.CancellableTask;
import rice.p2p.commonapi.Endpoint;
import rice.p2p.commonapi.Id;
import rice.p2p.commonapi.Message;
import rice.p2p.commonapi.Node;
import rice.p2p.commonapi.NodeHandle;
import rice.p2p.commonapi.RouteMessage;
import rice.p2p.scribe.Scribe;
import rice.p2p.scribe.ScribeContent;
import rice.p2p.scribe.ScribeImpl;
import rice.p2p.scribe.ScribeMultiClient;
import rice.p2p.scribe.Topic;
import rice.pastry.NodeIdFactory;
import rice.pastry.PastryNode;
import rice.pastry.PastryNodeFactory;
import rice.pastry.commonapi.PastryIdFactory;
import rice.pastry.socket.SocketPastryNodeFactory;
import rice.pastry.standard.RandomNodeIdFactory;

public class PastryApplication implements Application, ScribeMultiClient {

	public static final int DEFAULT_PORT = 6464;

	final Environment env = new Environment();

	/**
	 * The Endpoint represents the underlying node. By making calls on the
	 * Endpoint, it assures that the message will be delivered to a MyApp on
	 * whichever node the message is intended for.
	 */
	protected Endpoint endpoint;

	/**
	 * The node we were constructed on.
	 */
	protected Node node;

	/**
	 * My immediate best friend.
	 */
	private NodeHandle immediateBuddy;

	/**
	 * The only topic we currently talk about.
	 */
	private Topic movieTopic;

	/**
	 * This task kicks off publishing and anycasting. We hold it around in case
	 * we ever want to cancel the publishTask.
	 */
	CancellableTask publishTask;

	/**
	 * My handle to a scribe impl.
	 */
	Scribe myScribe;

	private String remoteHost;
	private int remotePort;
	private int localPort;

	private KApplication app;

	/**
	 * Creates a new pastry application.
	 * 
	 * The pastry application handles all aspects of P2P communication and only
	 * talks to the main application to inform of incoming messages.
	 * 
	 * This constructor enforces that the connection be established to a remote
	 * ring, otherwise the construction fails.
	 * 
	 * @param ring
	 *            The ring address to connect to, if null then we establish a
	 *            new ring on the local network.
	 * 
	 * @param lport
	 *            An optional local port, when set to 0 the port is dynamically
	 *            selected.
	 * @throws IOException
	 */
	public PastryApplication(KApplication application, String host, int port, int lport)
			throws Exception {

		/* Prepare connection information */
		if (lport == 0)
			lport = findFreePort();

		this.localPort = lport;
		this.remoteHost = host.trim();
		this.remotePort = port;

		/*
		 * if (remoteHost.equals("localhost") || remoteHost.equals("127.0.0.1")
		 * || remoteHost.equals("0.0.0.0")) { remoteHost =
		 * InetAddress.getLocalHost().getHostName(); }
		 */

		app = application;

		InetSocketAddress ring = new InetSocketAddress(remoteHost, remotePort);

		// disable the UPnP setting (in case you are testing this on a NATted
		// LAN)
		env.getParameters().setString("nat_search_policy", "never");
		env.getParameters().setBoolean("pastry_socket_increment_port_after_construction", false);
		env.getParameters().setInetAddress("socket_bindAddress",
				InetAddress.getByName(application.getSettings().getLocalHost()));

		NodeIdFactory nidFactory = new RandomNodeIdFactory(env);

		PastryNodeFactory factory = null;
		factory = new SocketPastryNodeFactory(nidFactory, lport, env);

		rice.pastry.NodeHandle bootHandle = ((SocketPastryNodeFactory) factory).getNodeHandle(ring);
		immediateBuddy = bootHandle;

		if (bootHandle == null) {
			throw new Exception("Could not connect to the remote node.");
		}

		localSetup(factory, bootHandle);
	}

	/**
	 * Creates a new pastry application.
	 * 
	 * The pastry application handles all aspects of P2P communication and only
	 * talks to the main application to inform of incoming messages.
	 * 
	 * This constructor is used to create a new ring from the local app.
	 * 
	 * @param lport
	 *            The port to boot the new ring, if 0 is chosen the port will be
	 *            auto generated.
	 * @throws IOException
	 */
	public PastryApplication(KApplication application, int lport) throws Exception {
		/* Prepare connection information */
		if (lport == 0)
			lport = findFreePort();

		this.localPort = lport;
		this.remoteHost = null;
		this.remotePort = -1;

		app = application;

		// disable the UPnP setting (in case you are testing this on a NATted
		// LAN)
		env.getParameters().setString("nat_search_policy", "never");
		env.getParameters().setBoolean("pastry_socket_increment_port_after_construction", false);
		env.getParameters().setInetAddress("socket_bindAddress",
				InetAddress.getByName(application.getSettings().getLocalHost()));

		NodeIdFactory nidFactory = new RandomNodeIdFactory(env);

		PastryNodeFactory factory = null;
		factory = new SocketPastryNodeFactory(nidFactory, lport, env);

		localSetup(factory, null);
	}

	/**
	 * Setup the local node.
	 * 
	 * This prepares the local node and endpoint.
	 * 
	 * When the provided bootHandle is null this also creates a new ring on the
	 * local node.
	 * 
	 * @param factory
	 *            The node ID factory.
	 * @param bootHandle
	 *            The handle for the boot node.
	 * @throws IOException
	 */
	private void localSetup(PastryNodeFactory factory, rice.pastry.NodeHandle bootHandle)
			throws IOException {
		// construct a node, passing the null boothandle on the first loop will
		// cause the node to start its own ring
		PastryNode node = factory.newNode();
		node.boot(bootHandle == null ? null : new InetSocketAddress(remoteHost, remotePort));

		immediateBuddy = node.getLeafSet().get(0);

		if (immediateBuddy == null) {
			immediateBuddy = node.getLocalNodeHandle();
		}

		try {

			// the node may require sending several messages to fully boot into
			// the
			// ring
			synchronized (node) {
				while (!node.isReady() && !node.joinFailed()) {
					// delay so we don't busy-wait
					try {
						node.wait(100);
					} catch (InterruptedException e) {
					}

					// abort if can't join
					if (node.joinFailed()) {
						throw new IOException("Could not join the FreePastry ring.  Reason:"
								+ node.joinFailedReason());
					}
				}
			}

			System.out.println("Finished creating new node " + node);

			this.node = node;

			// construct Scribe
			myScribe = new ScribeImpl(node, "myScribeInstance");

			// construct the topic
			movieTopic = new Topic(new PastryIdFactory(env), "MovieTopic");
			System.out.println("movieTopic = " + movieTopic);

			// Setup endpoint.
			endpoint = node.buildEndpoint(this, "PastryApplication");

			// now we can receive messages
			endpoint.register();

		} catch (Exception e) {
			node.destroy();
			env.destroy();
			throw e;
		}

		subscribe();
	}

	/**
	 * Subscribes to myTopic.
	 */
	private void subscribe() {
		myScribe.subscribe(movieTopic, this, new ScribeMessageContent(
				new StringMessage("Greetings")), null);
	}

	public int getPort() {
		return localPort;
	}

	public String getRemoteHost() {
		return remoteHost;
	}

	public int getRemotePort() {
		return remotePort;
	}

	@Override
	public boolean forward(RouteMessage message) {
		return true;
	}

	public void disconnect() {
		((PastryNode) node).destroy();
		env.destroy();
	}

	/**
	 * Called when we receive a message.
	 */
	@Override
	public void deliver(Id id, Message message) {
		System.out.println("got " + message + " from " + id);

		if (message instanceof MovieMessage) {
			app.movieArrived(((MovieMessage) message).movie);
		}
	}

	/**
	 * Called when you hear about a new neighbour. Don't worry about this method
	 * for now.
	 */
	@Override
	public void update(NodeHandle handle, boolean joined) {
		System.out.println("Node " + handle + (joined ? " joined" : " left"));
	}

	private static int findFreePort() throws IOException {
		ServerSocket server = new ServerSocket(0);
		int port = server.getLocalPort();
		server.close();
		return port;
	}

	public void publishMessage(Message message) {
		myScribe.publish(movieTopic, new ScribeMessageContent(message));
	}

	@Override
	public boolean anycast(Topic topic, ScribeContent content) {
		return true;
	}

	@Override
	public void deliver(Topic topic, ScribeContent content) {
		System.out.println("got " + content + " on topic " + topic);

		if (content instanceof ScribeMessageContent) {
			Message message = ((ScribeMessageContent) content).getMessage();
			if (message instanceof MovieMessage) {
				app.movieArrived(((MovieMessage) message).movie);
			}
		}
	}

	@Override
	public void childAdded(Topic topic, NodeHandle child) {
	}

	@Override
	public void childRemoved(Topic topic, NodeHandle child) {
	}

	@Override
	public void subscribeFailed(Topic topic) {
		System.err.println("Failed to subscribe to topic " + topic);
		System.err.println("Retrying....");
		subscribe();
	}

	@Override
	public void subscribeFailed(Collection<Topic> topics) {
		// TODO Auto-generated method stub

	}

	@Override
	public void subscribeSuccess(Collection<Topic> topics) {
		// TODO Auto-generated method stub

	}
}
