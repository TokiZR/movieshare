package org.kinema;

import java.util.EventListener;

public interface StatusEventListener extends EventListener {

	public void onStatusChange(StatusEvent event);
	
}
