# Kinema #

## Bibliotecas necessárias

 - FreePastry _2.1_ (http://www.freepastry.org/FreePastry/)
 - Guava (https://code.google.com/p/guava-libraries/)
 - Gson (https://code.google.com/p/google-gson/)
 - Apache Commons IO (http://commons.apache.org/proper/commons-io/)
 - Apache Commons Lang (http://commons.apache.org/proper/commons-lang/)
 - Mig Layout 4.0 (Swing) (http://www.miglayout.com/)

## Proposta

Sistema distribuído P2P para o compartilhamento de eventos cinematográficos.

### Funcionalidades

Gerenciamento das conexões remotas, preferências de gênero e tópicos e compartilhamento de filmes.

### Disfuncionalidades

Devido a largura do domínio dos tópicos não é possível utilizar o serviço de Pub/Sub do Scribe (o mesmo é usado apenas como um grupo para _broadcast_).

O FreePastry identifica os nós pelos seus pares IP/Porta, de modo que o IP que os nós utilizam para se comunicar tem que ser o mesmo dentro e fora de suas redes locais. A implicação disso é apenas que ao se comunicar com um nó na rede local deve-se usar o IP externo da rede e algum mecanismo de _Port Forwarding_ no roteador para que se obtenha uma conexão.

Existe um suporte a _UPnP_ e múltiplos identificadores por nó dentro do FreePastry a partir da versão 2.1 mas esta implementação tem a dependência de que pelo menos 5% dos nós da rede sejam acessíveis a partir de um endereço de internet sem _NAT_ (ou com _Port Forwarding_ propriamente configurado) o que dificulta sua configuração e teste. Além disso a documentação não é clara sobre como os nós devem ser criados e configurados.

## Tecnologias

 - Java : Plataforma
 - FreePastry/Scribe : Comunicação
 - Swing : Interface

## Código Fonte

O código fonte está disponível em https://bitbucket.org/TokiZR/movieshare/.
Uma versão compilada e contendo as bibliotecas está disponível na seção de downloads do mesmo site.
